# AiryscanJ

The folder AiryscanJ is a Fiji package to run Airyscan image resconstruction as a Fiji plugin.
This package contains binaries for Windows and MacOSX. For any other system, you
need to compile the sources from https://gitlab.inria.fr/serpico/airyscan

# Install

Copy paste the AiryscanJ directory in the Fiji.app/plugins folder