var m_batch;
var m_d = 1;
var m_registration_method;

macro "airyscanregister"{

	// settings dialog
	Dialog.create("Airyscan Register");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("Fiji macro to co-register 32 channels \n (czi format) images from the Airyscan microscope");
	
	// registration
	Dialog.addMessage("__________________________________________________");
	Dialog.addMessage("REGISTRATION:");
	Dialog.addChoice("      Method", newArray("stackReg", "coefs") );
  	Dialog.addNumber("          d:", 1);

 	// batch processing
 	Dialog.addMessage("\n");
 	Dialog.addMessage("__________________________________________________");
 	Dialog.addMessage("BATCH PROCESSING:\n");
 	Dialog.addCheckbox("Batch", false);
 	Dialog.show();

 	// get settings
 	m_registration_method = Dialog.getChoice();
 	m_d = Dialog.getNumber();
  	m_batch = Dialog.getCheckbox();

	setBatchMode(true);
	if (m_batch){
		
		run("Close All");
		dir = getDirectory("input directory");
		outputdir = getDirectory("output directory");
		files = getFileList(dir);
		for (i=0 ; i < files.length ; i++){
			if (endsWith(".czi") ){
				open(dir + files[i]);
				process();
				saveAs("TIFF", files[i]);
				run("Close All");
			}
		}
		
	}
	else{
		process();
	}
	setBatchMode(false);
}

function process(){

	// Duplicate the stack
	originalImageName = getTitle();
	getDimensions(width, height, channels, slices, frames);
	run("Duplicate...", "duplicate channels=1-"+ toString(channels/2));
	rename("REG_" + originalImageName);
	stackTitle = getTitle();

	if (slices > 1)
	{
		for (c = 1; c <= channels/2; c++) 
		{
			for (s = 1; s <= slices; s++) 
			{
				Stack.setChannel(c);
				Stack.setSlice(s);
				register(stackTitle);
			}
		}
	}
	else
	{
		for (c = 1; c <= channels/2; c++) 
		{
			Stack.setChannel(c);
			register(stackTitle);
		}
	}
}

function register(imageTitle){
	if (m_registration_method == "stackReg"){
		register_stackreg(imageTitle);
	}
	else{
		register_translate(imageTitle);
	}
}

function register_stackreg(originalStack){
		// get target images
	path = getDirectory("temp");
	
	Stack.setFrame(1);
	run("Duplicate...", "use");
	run("32-bit");
	targetImage = "targetimage.tif";
	rename(targetImage);
	run("Tiff...", "save=" + path + targetImage);
	close();

	// Estimate transformations
	translations = newArray();
	for ( t=2 ; t <= 32 ; t++)
	{
		run("Clear Results"); 
	
		selectWindow(originalStack);
		Stack.setFrame(t);
		run("Duplicate...", "use");
		run("32-bit");
		sourceImage = "sourceimage.tif";
		rename(sourceImage);
	
		
		// save images to tmp
		selectWindow("sourceimage.tif");
		run("Tiff...", "save=" + path + sourceImage);
		
		// registration
		width = getWidth();
		height = getHeight();
		
		run("TurboReg ",
			"-align " // Register the two images that we have just prepared.
			+ "-window " + sourceImage + " "// Source (window reference).
			+ "0 0 " + (width - 1) + " " + (height - 1) + " " // No cropping.
			+ "-file " + path + targetImage + " "// Target (file reference).
			+ "0 0 " + (width - 1) + " " + (height - 1) + " " // No cropping.
			+ "-rigidBody " // This corresponds to rotation and translation.
			+ (width / 2) + " " + (height / 2) + " " // Source translation landmark.
			+ (width / 2) + " " + (height / 2) + " " // Target translation landmark.
			+ "0 " + (height / 2) + " " // Source first rotation landmark.
			+ "0 " + (height / 2) + " " // Target first rotation landmark.
			+ (width - 1) + " " + (height / 2) + " " // Source second rotation landmark.
			+ (width - 1) + " " + (height / 2) + " " // Target second rotation landmark.
			+ "-showOutput"); // In case -hideOutput is selected, the only way to
			// retrieve the registration results is by the way of another plugin.
		
		// get output
		selectWindow("Output");
		//run("Duplicate...", "use");
		//rename(originalStack + "_registered_" + c + ".tif");
		close("Output");
		
		// calculate translation 
		sourceX0 = getResult("sourceX", 0); // First line of the table.
		sourceY0 = getResult("sourceY", 0);
		targetX0 = getResult("targetX", 0);
		targetY0 = getResult("targetY", 0);
		sourceX1 = getResult("sourceX", 1); // Second line of the table.
		sourceY1 = getResult("sourceY", 1);
		targetX1 = getResult("targetX", 1);
		targetY1 = getResult("targetY", 1);
		sourceX2 = getResult("sourceX", 2); // Third line of the table.
		sourceY2 = getResult("sourceY", 2);
		targetX2 = getResult("targetX", 2);
		targetY2 = getResult("targetY", 2);
		dxt = targetX0 - sourceX0;
		dyt = targetY0 - sourceY0;
		translation = sqrt(dxt * dxt + dyt * dyt); // Amount of translation, in pixel units.
		dx = sourceX2 - sourceX1;
		dy = sourceY2 - sourceY1;
		sourceAngle = atan2(dy, dx);
		dx = targetX2 - targetX1;
		dy = targetY2 - targetY1;
		targetAngle = atan2(dy, dx);
		rotation = targetAngle - sourceAngle; // Amount of rotation, in radian units.
		print("dx = " + dxt + ", dy = " + dyt + ", angle = " + (rotation * 180.0 / PI) );

		translations = Array.concat( translations, newArray(dxt, dyt) );	
		close (sourceImage);
	}

	// apply transformations
	for ( t=2 ; t <= 32 ; t++)
	{
		translate(t, translations[2*(t-2)], translations[2*(t-2)+1]);
	} 
}

function register_translate(imageTitle){

	selectWindow(imageTitle);

	d=m_d;

	translate(2, d, 0.5*d);
	translate(3, d, -0.5*d);
	translate(4, 0, -d);
	translate(5, -d, -0.5*d);
	translate(6, -d, 0.5*d);
	translate(7, 0, d);
	translate(8, d, 1.5*d);
	translate(9, 2*d, d);
	translate(10, 2*d, 0);
	translate(11, 2*d, -d);
	translate(12, d, -1.5*d);
	translate(13, 0, -2*d);
	translate(14, -d, -1.5*d);
	translate(15, -2*d, -d);
	translate(16, -2*d, 0);
	translate(17, -2*d, d);
	translate(18, -d, 1.5*d);
	translate(19, 0, 2*d);
	translate(20, d, 2.5*d);
	translate(21, 2*d, 2*d);
	translate(22, 3*d, 0.5*d);
	translate(23, 3*d, -0.5*d);
	translate(24, 2*d, -2*d);
	translate(25, d, -2.5*d);
	translate(26, -d, -2.5*d);
	translate(27, -2*d, -2*d);
	translate(28, -3*d, -0.5*d);
	translate(29, -3*d, 0.5*d);
	translate(30, -2*d, 2*d);
	translate(31, -1*d, 2.5*d);
	translate(32, 0, 3*d);
}

function translate(detector, x, y){
	Stack.setFrame(detector);
	run("Translate...", "x="+d2s(x,4)+" y="+d2s(y,4)+" interpolation=Bicubic slice");
}
