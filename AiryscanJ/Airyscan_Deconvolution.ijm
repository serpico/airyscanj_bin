var m_batch;
var m_reg_stack;
var m_deconvSigma;
var m_deconvRegularization;
var m_deconvWeighting;

macro "Airyscan_Deconvolution"
{
	requires("1.53d");
	// settings dialog
	Dialog.create("Airyscan Processing");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("Airyscan: Deconvolution reconstruction", 18, "#0071C3");
	if ( nImages == 0 ){
		Dialog.addMessage("WARNING: No image open, using batch mode", 9, "#FFA500");
	}
	
	// registration
	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
	Dialog.addMessage("RECONSTRUCTION:", 14, "#0071C3");
	default_batch = true;
	if ( nImages > 0 ){
		Dialog.addImageChoice("       Co-registered stack");  
		default_batch = false;
	}
	Dialog.addNumber("Sigma:", 1.5, 1, 18, "");
	Dialog.addNumber("Regularization:", 12, 1, 18, "");
	Dialog.addChoice("Weighting", newArray(0.1, 0.6, 0.9), 0.6);

 	// batch processing
 	Dialog.addMessage("\n");
 	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
 	Dialog.addMessage("BATCH PROCESSING:", 14, "#0071C3");
 	Dialog.addCheckbox("Batch", default_batch);
 	Dialog.show();

 	// get settings
 	if ( nImages > 0 ){
 		m_reg_stack = Dialog.getImageChoice();
 	}
 	m_deconvSigma = Dialog.getNumber();
	m_deconvRegularization = Dialog.getNumber();
	m_deconvWeighting = Dialog.getChoice();
  	m_batch = Dialog.getCheckbox();

	if (m_batch){
		run("Close All");
		dir = getDirectory("input directory");
		outputdir = getDirectory("output directory");
		files = getFileList(dir);
		for (i=0 ; i < files.length ; i++){
			if (endsWith(".czi") ){
				open(dir + files[i]);
				m_reg_stack = getTitle();
				process();
				saveAs("TIFF", files[i]);
				run("Close All");
			}
		}
	}
	else{
		process();
	}
}

function process()
{
	selectWindow(m_reg_stack);
	imageTitle = getTitle();
	
	run("32-bit");
	getDimensions(width, height, channels, slices, frames);

	res_name = "DECONV_" + "r" + d2s(m_deconvRegularization, 0) + "_w" + d2s(m_deconvWeighting, 2) + "_s" + d2s(m_deconvSigma, 2) + "_" + imageTitle;

	if (slices > 1 && frames > 1){

		channel_str = "";
		for (c = 1; c <= channels; c++) {
			channel_str += "c" + toString(c) + "=channel"+toString(c) + " "; 
			for (s = 1; s <= slices; s++) {
				selectWindow(imageTitle);
				Stack.setChannel(c);
				Stack.setSlice(s);
				run("Duplicate...", "duplicate channels="+c+" slices="+s);
				im = getTitle();
				run_deconv(im);
				rename("DECONV_channel"+toString(c)+"_"+toString(s));
			}
			run("Images to Stack", "name=channel"+c+" title=[DECONV_] use");
		}
		channel_str += "create";
		if (channels > 1){
			run("Merge Channels...", channel_str);
		}
		rename(res_name);
	}
	else{
		channel_str = "";
		for (c = 1; c <= channels; c++) {
			channel_str += "c" + toString(c) + "=channel"+toString(c) + " "; 
			selectWindow(imageTitle);
			Stack.setChannel(c);
			run("Duplicate...", "duplicate channels="+c);
			im = getTitle();
			run_deconv(im);
			rename("channel"+toString(c));
		}
		if (channels > 1){
			channel_str += "create";
			run("Merge Channels...", channel_str);
		}
		rename(res_name);
	}
	if (channels > 1){
		Stack.setDisplayMode("color");
	}
}

function run_deconv(imageTitle){
	tmpPath = getDirectory("plugins");
	saveAs("tiff", tmpPath + "tmp.tif");
	exe_name = "sdeconv2d";
	if (startsWith(getInfo("os.name"), "Windows")) {
		exe_name += ".exe";
	}
	execPath = getDirectory("plugins") + "AiryscanJ"+File.separator+exe_name;
	exec(execPath, "-i", tmpPath + "tmp.tif", "-o", tmpPath + "tmp_deconv.tif", "-sigma", m_deconvSigma, "-regularization", m_deconvRegularization, "-weighting", m_deconvWeighting, "-stack", "True", "-scoeff", "airyscand2c");
	open(tmpPath + "tmp_deconv.tif");
	close("tmp.tif");

	File.delete(tmpPath + "tmp.tif"); 
	File.delete(tmpPath + "tmp_deconv.tif");
}
