var m_pinhole = 0;
var m_batch = false;

macro "Airyscan_PseudoConfocal"
{
	// settings dialog
	Dialog.create("Airyscan Processing");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("       Airyscan: Pseudo-Confocal reconstruction", 18, "#0071C3");
	if ( nImages == 0 ){
		Dialog.addMessage("WARNING: No image open, using batch mode", 9, "#FFA500");
	}
	
	// registration
	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
	Dialog.addMessage("RECONSTRUCTION:", 14, "#0071C3");
	Dialog.addChoice("Pinhole", newArray("1.25", "1", "0.6"), "1");
  	
 	// batch processing
 	default_batch = true;
	if ( nImages > 0 ){ 
		default_batch = false;
	}
 	Dialog.addMessage("\n");
 	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
 	Dialog.addMessage("BATCH PROCESSING:", 14, "#0071C3");
 	Dialog.addCheckbox("Batch", default_batch);
 	Dialog.show();

 	// get settings
 	m_pinhole = Dialog.getChoice();
  	m_batch = Dialog.getCheckbox();

	if (m_batch){
		run("Close All");
		dir = getDirectory("input directory");
		outputdir = getDirectory("output directory");
		files = getFileList(dir);
		for (i=0 ; i < files.length ; i++){
			if (endsWith(".czi") ){
				open(dir + files[i]);
				process();
				saveAs("TIFF", files[i]);
				run("Close All");
			}
		}
	}
	else{
		process();
	}
}

function process(){
	imageTitle = getTitle();
	if (m_pinhole == "1.25"){
		pseudo_confocal(imageTitle, 32, "1.25");
	}
	else if (m_pinhole == "1"){
		pseudo_confocal(imageTitle, 19, "1");
	}
	else if (m_pinhole == "0.6"){
		pseudo_confocal(imageTitle, 7, "0.6");
	}
}

function pseudo_confocal(imageTitle, frame_idx, pinhole)
{
	selectWindow(imageTitle);
	run("32-bit");
	getDimensions(width, height, channels, slices, frames);

	if (slices > 1 && frames > 1){
		run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
		run("Z Project...", "stop="+frame_idx+" projection=[Sum Slices] all");
		run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
		Stack.setDisplayMode("color");
		rename("PCONFOCAL_"+pinhole+"_" + imageTitle);
		out_image = getTitle();

		// reset the original stack
		selectWindow(imageTitle);
		run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
		Stack.setDisplayMode("color");
		selectWindow(out_image);
	}
	else{
		run("Z Project...", "stop="+frame_idx+" projection=[Sum Slices]");
		rename("PCONFOCAL_"+pinhole+"_" + imageTitle);
	}
}
